# Configurações para ambiente de desenvolvimento
API_KEY=[sua chave Open Exchange Rates API]
BASE_RATE=USD

# DEBUG=True ativa o modo de depuração, recomendado para desenvolvimento
DEBUG=True

# ALLOWED_HOSTS especifica os hosts permitidos durante o desenvolvimento
ALLOWED_HOSTS=localhost,127.0.0.1

# Para produção, ajuste as configurações da seguinte maneira:
# DEBUG=False desativa o modo de depuração em um ambiente de produção
# ALLOWED_HOSTS deve incluir os hosts permitidos para o ambiente de produção
# API_KEY e BASE_RATE podem ser ajustados conforme necessário para produção

