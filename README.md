# CoinSnark: Conversor de Moedas em Python/Django


CoinSnark, um Conversor de Moedas desenvolvido em Python/Django. Essa aplicação robusta facilita a conversão monetária entre diversas moedas, proporcionando resultados precisos e eficientes. 

Para garantir a precisão das informações, o CoinSnark consome dados em tempo real da [Open Exchange Rates API](https://openexchangerates.org/). Para começar a utilizar a API, é necessário criar uma conta na Open Exchange Rates e obter uma chave de API para realizar requisições de dados atualizados.

## Funcionalidades Principais

- **Conversões Simples:** A API permite a conversão direta entre diferentes moedas, incluindo USD, BRL, EUR, BTC e ETH.
- **Parâmetros Flexíveis:** Através de parâmetros de requisição simples, como moeda de origem (`from`), moeda final (`to`) e o valor a ser convertido (`amount`), é possível obter resultados rápidos e precisos.

## Execução da Aplicação


1. **Clone o Repositório:**

   ```bash
   git clone $seu-fork
   cd $seu-fork
   ```

2. **Instale as Dependências:**

   ```bash
   pip install -r requirements.txt
   ```

3. **Inicie a Aplicação:**

   ```bash
   cd coinsnark
   python manage.py runserver
   ```

## API Endpoints

Experimente as funcionalidades da API através de endpoints simples:

- **Conversão Monetária:**

  ```
  GET /converter/?from=<moeda_origem>&to=<moeda_destino>&amount=<valor>
  ```

  Exemplo:

  ```
  curl "http://127.0.0.1:8000/api/convert/?from=BRL&to=EUR&amount=5000"
  ```

- **Todas as Taxas Suportadas:**

  Exemplo:

  ```
  curl http://127.0.0.1:8000/api/all_rates/
  ```

## Vulnerabilidades

Processos de autenticação de tokens precisa ser implementado.
Sistema de monitoria e logs, também precisa ser implementado.


## Contribuições

Contribuições são sempre bem-vindas! Sinta-se à vontade para fazer um fork e enviar pull requests.


## Contato

Se você gostou do que viu e se interessa pelo tema, não hesite em fazer contato.
<br />
<p align="center">
<a href="https://gitlab.com/rmotta.net"><img src="https://img.shields.io/badge/Gitlab--_.svg?style=social&logo=gitlab" alt="GitLab"></a>
<a href="https://github.com/rmottanet"><img src="https://img.shields.io/badge/Github--_.svg?style=social&logo=github" alt="GitHub"></a>
<a href="https://instagram.com/rmottanet/"><img src="https://img.shields.io/badge/Instagram--_.svg?style=social&logo=instagram" alt="Instagram"></a>
<a href="https://www.linkedin.com/in/rmottanet/"><img src="https://img.shields.io/badge/Linkedin--_.svg?style=social&logo=linkedin" alt="Linkedin"></a>
</p>

