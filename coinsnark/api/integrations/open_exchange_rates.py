import os
import json
import requests
from cachetools import TTLCache
from urllib.parse import urlencode
from decouple import config

# env session
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'coinsnark.settings')
api_key = config('API_KEY')
base_rate = config('BASE_RATE', default='USD')

# cache session
cache = TTLCache(maxsize=1000, ttl=3600)

# exceptions Open Exchange Rates 
class ExternalAPIError(Exception):
    pass

def get_exchange_rates():
    # verifica se as taxas estão no cache
    cached_rates = cache.get('exchange_rates')
    if cached_rates:
        return cached_rates

    # action
    base_url = "https://openexchangerates.org/api/latest.json"
    
    params = {
        "app_id": api_key,
        "base": base_rate,
        "prettyprint": "false",
        "show_alternative": "true"
    }
    
    url = f"{base_url}?{urlencode(params)}"

    headers = {"accept": "application/json"}

    try:
        response = requests.get(url, headers=headers)

        # checa resposta (código de status 200)
        response.raise_for_status()

        data = response.json()
        exchange_rates = {"timestamp": data["timestamp"], "rates": data["rates"]}
        
        cache['exchange_rates'] = exchange_rates

        return exchange_rates
    except requests.exceptions.RequestException as e:
        # lida com erros de solicitação
        print(f"Erro na solicitação da API externa: {e}")
        raise ExternalAPIError("Failed to fetch exchange rates from the external API")
    except json.JSONDecodeError as e:
        # lida com erros de decodificação JSON
        print(f"Erro na decodificação JSON: {e}")
        raise ExternalAPIError("Failed to decode JSON from the external API")
