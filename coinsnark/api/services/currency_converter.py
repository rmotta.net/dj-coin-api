from ..utils.validator import validate_currency_code, validate_amount
from ..integrations.open_exchange_rates import get_exchange_rates
from decimal import Decimal, ROUND_HALF_UP
from datetime import datetime
import pytz


class CurrencyConversionError(Exception):
    pass
    
def convert_currency(amount, from_currency, to_currency):
	# validação de entrada
	if not isinstance(amount, (int, float, Decimal)) or not isinstance(from_currency, str) or not isinstance(to_currency, str):
		return {"error": "Invalid input parameters"}
	
	if not (validate_amount(amount) and validate_currency_code(from_currency) and validate_currency_code(to_currency)):
		return {"error": "Invalid input parameters"}
	
	try:
		rates_data = get_exchange_rates()
		rates = rates_data["rates"]
		timestamp_utc = datetime.utcfromtimestamp(rates_data["timestamp"])
		timestamp_utc = timestamp_utc.replace(tzinfo=pytz.UTC)
	
		if from_currency in rates and to_currency in rates:
			conversion_rate = rates[to_currency] / rates[from_currency]
			converted_amount = Decimal(amount) * Decimal(conversion_rate)
			converted_amount = converted_amount.quantize(Decimal('0.0001'), rounding=ROUND_HALF_UP)
			return {"amount": str(converted_amount), "from_currency": from_currency, "to_currency": to_currency, "timestamp": timestamp_utc.strftime('%Y-%m-%d %H:%M:%S UTC')}
		else:
			return {"error": f"Invalid currency codes: {from_currency} to {to_currency}"}
	except Exception as e:
		return {"error": f"Failed to fetch exchange rates: {str(e)}"}
	
