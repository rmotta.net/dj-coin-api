from django.test import TestCase
from django.urls import reverse
from ..services.external_api import get_exchange_rates
from ..services.currency_converter import convert_currency

class ServicesTests(TestCase):
	def test_currency_converter(self):
		url = reverse('convert')
		
		# teste com dados válidos
		valid_data = {'from': 'USD', 'to': 'EUR', 'amount': '123.45'}
		response = self.client.get(url, valid_data)
		converted_data = response.json()
		
		# verifica se a resposta tem as chaves esperadas
		expected_keys = {'amount', 'from_currency', 'to_currency', 'timestamp'}
		self.assertSetEqual(set(converted_data.keys()), expected_keys)
		
		# teste com dados inválidos (amount não é um número)
		invalid_amount_data = {'from': 'USD', 'to': 'EUR', 'amount': 'invalid'}
		response_invalid_amount = self.client.get(url, invalid_amount_data)
		invalid_amount_data = response_invalid_amount.json()
		
		# verifica se a resposta indica um erro para dados de entrada inválidos
		self.assertIn('error', invalid_amount_data)
		
		# teste com dados inválidos (moeda de origem ou destino não é uma string)
		invalid_currency_data = {'from': 123, 'to': 'EUR', 'amount': '123.45'}
		response_invalid_currency = self.client.get(url, invalid_currency_data)
		invalid_currency_data = response_invalid_currency.json()
		
		# verifica se a resposta indica um erro para dados de entrada inválidos
		self.assertIn('error', invalid_currency_data)
