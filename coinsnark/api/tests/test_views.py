from django.test import TestCase
from django.urls import reverse

class ViewsTests(TestCase):
    def test_api_welcome_message(self):
        response = self.client.get(reverse('welcome'))
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, {"message": "Welcome to dJ Coin API"})

    def test_api_all_exchange_rates(self):
        response = self.client.get('/api/all_rates/')
        self.assertEqual(response.status_code, 200)
        self.assertIn('timestamp', response.json())
        self.assertIn('rates', response.json())
	
