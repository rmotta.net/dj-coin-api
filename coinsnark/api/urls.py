from django.urls import path
from .views.views import welcome_message
from .views.currency import all_exchange_rates, convert

urlpatterns = [
    path('', welcome_message, name='welcome'),
    path('all_rates/', all_exchange_rates, name='all_rates'),
    path('convert/', convert, name='convert'),
]
