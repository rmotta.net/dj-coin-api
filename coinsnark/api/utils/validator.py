import re

def validate_currency_code(code):
    return bool(re.match(r'^[a-zA-Z]{3}$', code))

def validate_amount(amount):
    amount_str = str(amount)
    return bool(re.match(r'^\d+(\.\d+)?$', amount_str))
