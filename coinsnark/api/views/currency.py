# djcoin/api/views/currency.py
from django.http import JsonResponse
from ..integrations.open_exchange_rates import get_exchange_rates, ExternalAPIError
from ..services.currency_converter import convert_currency, CurrencyConversionError

# get all rates
def all_exchange_rates(request):
    rates_data = get_exchange_rates()

    if rates_data:
        return JsonResponse(rates_data, status=200)
    else:
        return JsonResponse({"error": "Failed to fetch exchange rates"}, status=500)

# convert currency
def convert(request):
    amount = request.GET.get('amount')
    from_currency = request.GET.get('from')
    to_currency = request.GET.get('to')

    if not all([amount, from_currency, to_currency]):
        return JsonResponse({"error": "Missing required parameters"}, status=400)

    try:
        amount = float(amount)
    except ValueError:
        return JsonResponse({"error": "Invalid amount"}, status=400)

    try:
        result = convert_currency(amount, from_currency.upper(), to_currency.upper())
    except ExternalAPIError as e:
        return JsonResponse({"error": str(e)}, status=500)
    except CurrencyConversionError as e:
        return JsonResponse({"error": str(e)}, status=400)

    if "error" in result:
        return JsonResponse(result, status=400)
    else:
        return JsonResponse(result, status=200)
