# api/views/views.py
from django.http import JsonResponse

def welcome_message(request):
    data = {'message': 'Welcome to dJ Coin API'}
    response = JsonResponse(data, status=200)
    return response
